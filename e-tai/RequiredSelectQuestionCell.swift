//
//  RequiredSelectQuestionCell.swift
//  e-tai
//
//  Created by 村岡 龍治 on 2017/08/20.
//  Copyright © 2017年 personal. All rights reserved.
//

import UIKit

class RequiredSelectQuestionCell: UITableViewCell {
    
    @IBOutlet weak var questionLabel : UILabel!
    
    @IBOutlet weak var answerSegmentedControl : UISegmentedControl!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
