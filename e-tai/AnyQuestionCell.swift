//
//  AnyQuestionCell.swift
//  e-tai
//
//  Created by 村岡 龍治 on 2017/08/20.
//  Copyright © 2017年 personal. All rights reserved.
//

import UIKit

class AnyQuestionCell: UITableViewCell {
    
    @IBOutlet weak var questionTextView : UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.questionTextView.layer.borderColor = UIColor.black.cgColor
        self.questionTextView.layer.borderWidth = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
