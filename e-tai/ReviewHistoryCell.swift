//
//  ReviewHistoryCell.swift
//  e-tai
//
//  Created by 村岡 龍治 on 2017/08/20.
//  Copyright © 2017年 personal. All rights reserved.
//

import UIKit

class ReviewHistoryCell: UITableViewCell {
    
    @IBOutlet weak var  shopNameLabel: UILabel!
    
    @IBOutlet weak var  alreadyReadLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
