//
//  ViewController.swift
//  e-tai
//
//  Created by 村岡 龍治 on 2017/08/19.
//  Copyright © 2017年 personal. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView : UITableView!
    
    @IBOutlet weak var signUpButton : UIBarButtonItem!
    
    var shopNameList: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        Alamofire.request("https://maps.googleapis.com/maps/api/place/textsearch/json?query=restaurant&location=35.6953411,139.7501469&radius=300&key=AIzaSyAO5QgcADHOynVyJfR8hgO8h1Ic-5EI0LI").responseJSON { response in
            
            guard let object = response.result.value else {
                return
            }
            
            let json = JSON(object)
            let shopList: Array? = json["results"].array
            
            if let shopListUnWrapped = shopList {
                for shop in shopListUnWrapped {
                    self.shopNameList.append(shop["name"].stringValue)
                }
                self.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.shopNameList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->  UITableViewCell {
        
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "Cell")
        cell.textLabel?.text = self.shopNameList[indexPath.row]
        
        return cell
    }
    
    func tableView(_ table: UITableView,didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showAnswerInput",sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        if (segue.identifier == "showAnswerInput") {
            let navigationController = segue.destination as? AnswerInputViewController
        }
    }
}

