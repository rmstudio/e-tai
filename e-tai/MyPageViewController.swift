//
//  MyPageViewController.swift
//  e-tai
//
//  Created by 村岡 龍治 on 2017/08/20.
//  Copyright © 2017年 personal. All rights reserved.
//

import UIKit

class MyPageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView : UITableView!
    
    @IBOutlet weak var profileView : UIView!
    
    @IBOutlet weak var userImageView : UIImageView!
    
    @IBOutlet weak var userNameLabel : UILabel!
    
    @IBOutlet weak var pointLabel : UILabel!
    
    var currentViewSize: CGSize!
    
    var shopNameList: [String]!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.currentViewSize = self.view.frame.size
        
        self.profileView.frame = CGRect(x:0,
                                      y:64,
                                      width:currentViewSize.width,
                                      height:200)
        
        self.userImageView.frame = CGRect(x:currentViewSize.width/2-71,
                                        y:20,
                                        width:141,
                                        height:100)
        
        self.userNameLabel.frame = CGRect(x:currentViewSize.width/2-53,
                                          y:self.userImageView.frame.origin.y + 110,
                                          width:107,
                                          height:21)
        
        self.pointLabel.frame = CGRect(x:currentViewSize.width/2-40,
                                       y:self.userNameLabel.frame.origin.y + 31,
                                       width:81,
                                       height:21)
        
        self.tableView.frame = CGRect(x:0,
                                      y:264,
                                      width:currentViewSize.width,
                                      height:currentViewSize.height)
        
        self.shopNameList = ["東京厨房九段南店", "吉野家 神保町店", "ピッツェリア ジオ ピッポ"]
        
        self.tableView.register(UINib(nibName: "ReviewHistoryCell", bundle: nil), forCellReuseIdentifier: "reviewHistory")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "過去のいいタイ！"
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->  UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "reviewHistory", for: indexPath) as! ReviewHistoryCell
        cell.shopNameLabel?.text = self.shopNameList[indexPath.row]
        
        return cell
    }
    
    func tableView(_ table: UITableView,didSelectRowAt indexPath: IndexPath) {
        //performSegue(withIdentifier: "showAnswerInput",sender: nil)
    }
    
    @IBAction func dismissUserEditView() {
        self.dismiss(animated: true, completion: nil)
    }

}
