//
//  AnswerInputViewController.swift
//  e-tai
//
//  Created by 村岡 龍治 on 2017/08/19.
//  Copyright © 2017年 personal. All rights reserved.
//

import UIKit

class AnswerInputViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var textView : UITextView!
    
    @IBOutlet weak var tableView : UITableView!

    var sendButton : UIButton!
    
    var currentViewSize: CGSize!
    
    var questionList: String!
    
    var shopName: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.sectionHeaderHeight = 50
        
        self.currentViewSize = self.view.frame.size
        
        self.tableView.frame = CGRect(x:0,
                                      y:0,
                                      width:currentViewSize.width,
                                      height:currentViewSize.height)
        
        self.tableView.isExclusiveTouch = false
        
        self.title = self.shopName
        
        self.tableView.register(UINib(nibName: "RequiredSelectQuestionCell", bundle: nil), forCellReuseIdentifier: "requiedSelect")
        self.tableView.register(UINib(nibName: "RequiredScoreQuestionCell", bundle: nil), forCellReuseIdentifier: "requiedScore")
        self.tableView.register(UINib(nibName: "AnyQuestionCell", bundle: nil), forCellReuseIdentifier: "anyQuestion")
        
        //self.setHeaderContents()
        
        self.setFooterContents()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0) {
            return 1
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0) {
            return 140
        } else {
            return 200
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->  UITableViewCell {
        
        if(indexPath.section == 0) {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "requiedSelect", for: indexPath) as! RequiredSelectQuestionCell
            cell.questionLabel.text = "接客態度はいかがでしたか？"
            cell.answerSegmentedControl.setTitle("悪い", forSegmentAt: 0)
            cell.answerSegmentedControl.setTitle("普通", forSegmentAt: 1)
            cell.answerSegmentedControl.setTitle("良い", forSegmentAt: 2)
            return cell
        } else {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "anyQuestion", for: indexPath) as! AnyQuestionCell
            return cell
        }
    }
    
    func tableView(_ table: UITableView,didSelectRowAt indexPath: IndexPath) {
        //performSegue(withIdentifier: "showAnswerInput",sender: nil)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section == 0) {
            return "必須回答項目"
        } else {
            return "任意回答項目"
        }
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    // MARK: button action
    
    func sendButtonDidiPushed(sender: UIButton) {
        performSegue(withIdentifier: "sendAnswer",sender: nil)
    }

    // MARK: private
    
    func setHeaderContents() {
        let headerCell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "header")!
        let headerView: UIView = headerCell.contentView
        self.tableView.tableHeaderView = headerView
        
        let inputNoticeText: UILabel = UILabel(frame: CGRect.zero)
        inputNoticeText.frame.size = CGSize(width: self.currentViewSize.width,
                                            height:headerView.frame.size.height);
        inputNoticeText.center = headerView.center
        inputNoticeText.text = "お店にいいタイ！"
        inputNoticeText.textAlignment = NSTextAlignment.center
        headerView.addSubview(inputNoticeText)
    }
    
    func setFooterContents() {
        let footerCell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "footer")!
        let footerView: UIView = footerCell.contentView
        self.tableView.tableFooterView = footerView
        
        self.sendButton = UIButton(type: .system)
        self.sendButton.frame.size = CGSize(width: 300,
                                       height:70);        
        self.sendButton.center = CGPoint(x:210, y: 47)
        self.sendButton.setTitle("いいタイ！", for: .normal)
        self.sendButton.titleLabel?.font = UIFont.systemFont(ofSize: 48)
        self.sendButton.addTarget(self, action: #selector(self.sendButtonDidiPushed(sender:)), for: .touchUpInside)
        footerView.addSubview(self.sendButton)
    }
}
