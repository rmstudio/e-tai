//
//  UserEditViewController.swift
//  e-tai
//
//  Created by 村岡 龍治 on 2017/08/19.
//  Copyright © 2017年 personal. All rights reserved.
//

import UIKit

class UserEditViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissUserEditView() {
        self.dismiss(animated: true, completion: nil)
    }

}
